senlin-dashboard (6.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 10:54:44 +0200

senlin-dashboard (6.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:39:25 +0200

senlin-dashboard (6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed 3 patches applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Sep 2022 10:04:42 +0200

senlin-dashboard (5.0.0-2) unstable; urgency=medium

  * Add Django 4 compat patches (Closes: #1015105):
    - django-4-ugettext_lazy-is-removed.patch
    - django-4-django.conf.urls.url-is-removed.patch

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Jul 2022 23:26:48 +0200

senlin-dashboard (5.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:28:28 +0200

senlin-dashboard (5.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add Fixed_Unit_test_for_senlin-dashboard.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Mar 2022 09:32:14 +0200

senlin-dashboard (5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 21:26:14 +0100

senlin-dashboard (4.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 21:30:33 +0200

senlin-dashboard (4.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:56:26 +0200

senlin-dashboard (4.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (Build-)depends on minimum horizon >= 20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 17:16:32 +0200

senlin-dashboard (3.0.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 16:33:55 +0200

senlin-dashboard (3.0.0-3) experimental; urgency=medium

  * Add rm_conffile to remove old files in /etc/openstack-dashboard/enable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 12:44:01 +0200

senlin-dashboard (3.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 May 2021 16:38:19 +0200

senlin-dashboard (3.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 17:34:04 +0200

senlin-dashboard (3.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Sat, 27 Mar 2021 21:14:58 +0100

senlin-dashboard (2.0.0-2) unstable; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Jul 2021 12:15:55 +0200

senlin-dashboard (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 17:51:21 +0200

senlin-dashboard (2.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2020 20:54:00 +0200

senlin-dashboard (1.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 18:30:52 +0200

senlin-dashboard (1.0.0~rc2-1) unstable; urgency=medium

  * Move the package to the horizon-plugins subgroup on Salsa.
  * New upstream release.
  * Uploading to unstable.
  * Fixed (build-)depends for this release.
  * Removed remove-broken-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 21:34:09 +0200

senlin-dashboard (0.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Add remove-broken-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Oct 2019 09:16:38 +0200

senlin-dashboard (0.10.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 22 Aug 2019 12:07:24 +0200

senlin-dashboard (0.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed package versions when satisfied in Buster.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 12:48:27 +0200

senlin-dashboard (0.9.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast

  [ Michal Arbet ]
  * Redesign senlin-dashboard:
      - Enabled files now in /etc/openstack-dashboard/
      - Removed post scripts which is now achieved by a trigger
  * d/copyright: Update copyright
  * d/control: Add me to uploaders field
  * d/control: Bump debhelper to 10
  * d/compat: Bump debhelper to 10
  * d/patches: Add install-missing-files.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 21 Jan 2019 21:09:48 +0100

senlin-dashboard (0.9.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 22:37:47 +0200

senlin-dashboard (0.9.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/rules: Removed UPSTREAM_GIT with default value

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Deprecating priority extra as per policy 4.0.1.

  [ Thomas Goirand ]
  * Using team+openstack@tracker.debian.org for maintainer.
  * New upstream release.
  * Switched to Python 3.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Jul 2018 13:55:47 +0200

senlin-dashboard (0.4.0-1) experimental; urgency=medium

  * Initial release. (Closes: #812964)

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 Sep 2016 13:43:27 +0200
